h2 t1)
skenaario 1:
1. Käyttäjä tarkastelee asiakasohjelmistoa graafisen käyttöliittymän kautta
2. Käyttäjä kirjautuu nimellä Paavo
3. Palvelin ilmoittaa käyttäjän Paavo sisäänkirjauksen
4. Käyttäjä valitsee keskustelunsa salaiseksi
5. Keskustelu alkaa käyttäjien Paavo ja Jussi välillä
6. Käyttäjä Paavo lopettaa keskustelun
7. Käyttäjä Paavo kirjautuu ulos
8. Palvelin ilmoittaa käyttäjän Paavo uloskirjauksen

skenaario 2:
1. Käyttäjä tarkastelee asiakasohjelmistoa graafisen käyttöliittymän kautta
2. Käyttäjä kirjautuu nimellä Anni
3. Palvelin ilmoittaa käyttäjän Anni sisäänkirjauksen
4. Käyttäjä valitsee keskustelunsa julkiseksi
5. Keskustelu alkaa käyttäjien Anni ja Jenni välillä
6. Toinen keskustelu alkaa käyttäjien Anni ja Kirsi välillä
7. Henkilöstöpäällikkö poistaa Jennin keskustelusta Anni ja Jenni, keskustelu päättyy
8. Anni lopettaa keskustelun Anni ja Kirsi
9. Käyttäjä Anni kirjautuu ulos
10. Palvelin ilmoittaa käyttäjän Anni uloskirjauksen

skenaario 3:
1. Käyttäjä tarkastelee asiakasohjelmistoa graafisen käyttöliittymän kautta
2. Käyttäjä kirjautuu nimellä Esa
3. Palvelin ilmoittaa käyttäjän Esa sisäänkirjauksen
4. Käyttäjä valitsee keskustelunsa julkiseksi
5. Keskustelu alkaa käyttäjien Esa ja Juho välillä
6. Henkilöstöpäällikkö jakaa Esalle väliaikaisesti oikeuden poistaa muita keskusteluista
7. Ylläpitäjä lähettää käyttäjille Esa ja Juho tiedotteen
8. Esa poistaa Juhon keskustelusta ja keskustelu päättyy
9. Esa kirjautuu ulos
10. Palvelin ilmoittaa käyttäjän Esa uloskirjauksen
